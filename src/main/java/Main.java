import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.Test;

import com.entity.Address;
import com.entity.Artist;
import com.entity.Book;
import com.entity.CD;
import com.entity.Person;

public class Main {

	@Test
	public void main() {
		// Person person = new Person("Valea", "Bekon", new Date());
		EntityManagerFactory managerFactory = Persistence
				.createEntityManagerFactory("punit");
		EntityManager entityManager = managerFactory.createEntityManager();

		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		Book book = new Book("Teodor", 12.4F, "O carte buna", "234234A", 234,
				true, new ArrayList<String>());
		CD cd = new CD();
		cd.setDescription("description");
		cd.setGenre("POP");
		cd.setMusicCompany("EVO");
		cd.setNumberOfCDs(23);
		cd.setPrice(324F);
		cd.setTitle("Title");
		cd.setTotalDuration(34F);
		cd.setTracks(new HashMap<Integer, String>());

		Artist artist = new Artist();
		artist.setFirstName("lArtist");
		artist.setLastName("lArtist");

		Artist artist1 = new Artist();
		artist1.setFirstName("lArtist1");
		artist1.setLastName("lArtist1");

		Artist artist12 = new Artist();
		artist12.setFirstName("lArtist2");
		artist12.setLastName("lArtist2");

		entityManager.persist(artist);
		entityManager.persist(artist1);
		entityManager.persist(artist12);
		List<Artist> artists = new ArrayList<>();
		artists.add(artist1);
		artists.add(artist12);
		artists.add(artist);

		// cd.setCreatedByArtists(artists);
		entityManager.persist(book);
		entityManager.persist(cd);
		List<CD> cds = new ArrayList<>();
		cds.add(cd);
		artist.setAppearsOnCDs(cds);
		entityManager.persist(artist);

		Person person = new Person("Kurchin", "Feodor", new Date());
		Address address = new Address();
		address.setCity("Chisinau");
		address.setCountry("Moldova");
		address.setState("Moldova");
		address.setStreet("Caela iesilor");
		address.setZipcode("34d");
		person.setAddress(address);

		// entityManager.persist(address);
		entityManager.persist(person);

		// dynamic query
		Query query = entityManager.createQuery("SELECT c FROM Person c");
		@SuppressWarnings("unchecked")
		List<Person> persons = query.getResultList();
		System.out.println("Result persons: " + persons.size());

		TypedQuery<Person> typedQuery = entityManager.createQuery(
				"SELECT c FROM Person c", Person.class);
		List<Person> list = typedQuery.getResultList();
		System.out.println("Result persons: " + list.size());

		Query namedQuery = entityManager.createNamedQuery("findPersonKurchin");
		TypedQuery<Person> typedNamedQuery = entityManager.createNamedQuery(
				"findPersonKurchin", Person.class);

		namedQuery.getFirstResult();
		typedNamedQuery.getResultList();

		// criteria api
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Person> criteriaQuery = builder.createQuery(Person.class);
		Root<Person> c = criteriaQuery.from(Person.class);
		criteriaQuery.select(c).where(
				builder.greaterThan(c.get("age").as(Integer.class), 40));

		Query cQuery = entityManager.createQuery(criteriaQuery);
		@SuppressWarnings("unchecked")
		List<Person> criteriaPersons = cQuery.getResultList();
		System.out.println("Criteria:" + criteriaPersons.size());
		tx.commit();
		// versionarea entitatii book1 in BD Optimistic Lock
		Book book1 = new Book("New title", 123F, "Short description", "sdfdf",
				234, true, new ArrayList<String>());
		tx.begin();
		entityManager.persist(book1);
		tx.commit();
		// Assert.assertEquals(Integer.valueOf(0), book1.getVersion());

		tx.begin();
		book1.raisePriceByTwoDollars();
		tx.commit();
		// Assert.assertEquals(Integer.valueOf(1), book1.getVersion());

		Cache cache = managerFactory.getCache();
		cache.evict(Person.class);
		entityManager.close();
		managerFactory.close();

	}
}
