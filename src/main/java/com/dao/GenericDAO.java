package com.dao;

import java.io.Serializable;
import java.util.List;

import javassist.NotFoundException;

import org.hibernate.HibernateException;

public interface GenericDAO<Entity, ID extends Serializable> {

    Entity get(String request, Object... params) throws HibernateException;

    Integer getCount(String request, Object... params)
	    throws HibernateException;

    Entity getByID(ID id) throws HibernateException, NotFoundException;

    List<Entity> getAll() throws HibernateException;

    Entity saveOrUpdate(Entity entity) throws HibernateException;

    void delete(Entity entity) throws HibernateException;

    int countAll() throws HibernateException;

    void update(Entity entity) throws HibernateException;
}
