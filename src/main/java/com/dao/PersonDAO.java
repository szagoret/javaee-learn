package com.dao;

import org.hibernate.Session;

import com.entity.Person;

public interface PersonDAO extends GenericDAO<Person, Integer> {
    public Session getSession();

}
