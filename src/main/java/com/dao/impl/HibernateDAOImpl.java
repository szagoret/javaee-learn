package com.dao.impl;

import java.io.Serializable;
import java.util.List;

import javassist.NotFoundException;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

import com.dao.GenericDAO;

/**
 * 
 * @author szagoret
 * 
 * <br />
 *         <b>HibernateDAOImpl</b> class implements common CRUD operations
 * 
 * @param <Entity>
 * @param <ID>
 */
public class HibernateDAOImpl<Entity, ID extends Serializable> implements
	GenericDAO<Entity, ID> {

    protected Class<Entity> entityClass;

    protected HibernateDAOImpl(Class<Entity> entityClass) {
	this.entityClass = entityClass;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Entity get(String request, Object... params)
	    throws HibernateException {
	Query query = this.createQuery(request, params);
	return (Entity) query.uniqueResult();
    }

    @Override
    public Integer getCount(String request, Object... params)
	    throws HibernateException {
	Query query = this.createQuery(request, params);
	return ((Long) query.iterate().next()).intValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Entity getByID(ID id) throws HibernateException, NotFoundException {
	Entity entity = (Entity) getSession().get(entityClass, id);
	if (entity == null) {
	    throw new NotFoundException("This entity is not found");
	}
	return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Entity> getAll() throws HibernateException {
	return getSession().createQuery("FROM " + entityClass.getName()).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Entity saveOrUpdate(Entity entity) throws HibernateException {
	return (Entity) getSession().merge(entity);
    }

    @Override
    public void delete(Entity entity) throws HibernateException {
	getSession().delete(entity);

    }

    @Override
    public int countAll() throws HibernateException {
	return ((Long) getSession().createQuery(
		"SELECT COUNT(*) FROM " + entityClass.getName()).uniqueResult())
		.intValue();
    }

    @Override
    public void update(Entity entity) throws HibernateException {
	getSession().update(entity);
    }

    public Query createQuery(String request, Object... params)
	    throws HibernateException {
	Query query = getSession().createQuery(request);

	for (int i = 0; i < params.length; i++) {
	    query.setParameter(i, params[i]);
	}
	return query;
    }

    protected Session getSession() {
	return SessionUtil.getSession();
    }

}
