package com.dao.impl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.Session;

import com.dao.PersonDAO;
import com.entity.Person;

public class PersonDAOImpl extends HibernateDAOImpl<Person, Integer> implements
	PersonDAO {
    Session session;

    public PersonDAOImpl() {
	super(Person.class);
	session = super.getSession();
    }

    @Override
    public Session getSession() {
	return session;
    }

    EntityManagerFactory factory = Persistence
	    .createEntityManagerFactory("punit");

}
