package com.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class SessionUtil {

    private final static SessionUtil instance = new SessionUtil();
    private final SessionFactory sessionFactory;

    private SessionUtil() {
	Configuration configuration = new Configuration().configure();
	StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
	builder.applySettings(configuration.getProperties());
	ServiceRegistry serviceRegistry = builder.build();
	sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    public static Session getSession() {
	return getInstance().sessionFactory.openSession();
    }

    private static SessionUtil getInstance() {
	return instance;
    }

}
