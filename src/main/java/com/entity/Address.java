package com.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;

/*
 * Tipul de acces la clasa imbricata este conditionata de clasa de baza,
 * daca aceasta este imbricate de catre mai multe clase atuci putem avea conflicte,
 * deaceea tipul de acces trebuie setat in mod explicit
 */

@Entity
@SecondaryTables({ @SecondaryTable(name = "city"),
	@SecondaryTable(name = "country") })
/* @Embeddable */
@Access(AccessType.FIELD)
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String street;

    @Column(table = "city")
    private String city;

    private String state;

    @Column(table = "city")
    private String zipcode;

    @Column(table = "country")
    private String country;

    /*
     * mappedBy indica ca detinatorul legaturii este obiectul Person
     */
    @OneToOne(mappedBy = "address")
    private Person person;

    @Column(nullable = false)
    public String getStreet() {
	return street;
    }

    public void setStreet(String street) {
	this.street = street;
    }

    @Column(nullable = false, length = 50)
    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    @Column(length = 3)
    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getZipcode() {
	return zipcode;
    }

    public void setZipcode(String zipcode) {
	this.zipcode = zipcode;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

}
