package com.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;

import com.listeners.PriceValidationListener;
import com.listeners.TitleValidateListener;

/*
 * atunci cind avem adnotari si fisiere XML cu configurari, intotdeauna 
 * prioritate are fisierul XML
 */
@EntityListeners({ PriceValidationListener.class, TitleValidateListener.class })
@Entity
// @DiscriminatorValue("B")
// @AttributeOverrides({
// // @AttributeOverride(name = "id", column = @Column(name = "book_id")),
// @AttributeOverride(name = "title", column = @Column(name = "book_title")),
// @AttributeOverride(name = "description", column = @Column(name =
// "book_description"))
//
// })
public class Book extends Item {
	/*
	 * @Id
	 * 
	 * @GeneratedValue private Long id;
	 */

	@Column(length = 500)
	private String isbn;
	private Integer nbOfPages;
	private Boolean illustrations;
	private String publisher;

	/**
	 * daca nu era adnotarea 'CollectionTable', atunci numele tabelului cu
	 * taguri trebuia sa fie 'BOOK_TAG' : [base tabel name] + _ + [field name]
	 */
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "Tag")
	@Column(name = "Value")
	private List<String> tags = new ArrayList<>();

	public Book() {
	}

	public Book(String title, Float price, String description, String isbn,
			Integer nbOfPages, Boolean illustrations, List<String> tags) {
		this.title = title;
		this.price = price;
		this.description = description;
		this.isbn = isbn;
		this.nbOfPages = nbOfPages;
		this.illustrations = illustrations;
		this.tags = tags;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Integer getNbOfPages() {
		return nbOfPages;
	}

	public void setNbOfPages(Integer nbOfPages) {
		this.nbOfPages = nbOfPages;
	}

	public Boolean getIllustrations() {
		return illustrations;
	}

	public void setIllustrations(Boolean illustrations) {
		this.illustrations = illustrations;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void raisePriceByTwoDollars() {
		this.price += 2;
	}

}
