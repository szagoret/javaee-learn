package com.entity;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class BookEJB {

	@PersistenceContext(unitName = "punit")
	private EntityManager em;

	public void createBook() {
		Book book = new Book("Book title", 12F, "Good description", "23423d",
				23, true, new ArrayList<String>());
		em.persist(book);
	}
}
