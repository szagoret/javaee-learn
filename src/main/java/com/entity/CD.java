package com.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;

@Entity
// @DiscriminatorValue(value = "C")
// @AttributeOverrides({
// @AttributeOverride(name = "id", column = @Column(name = "cd_id")),
// @AttributeOverride(name = "title", column = @Column(name = "cd_title")),
// @AttributeOverride(name = "description", column = @Column(name =
// "cd_description"))
//
// })
public class CD extends Item {

	private String musicCompany;
	private Integer numberOfCDs;
	private Float totalDuration;
	private String genre;

	@Lob
	private byte[] cover;

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "track")
	@MapKeyColumn(name = "position")
	@Column(name = "title")
	private Map<Integer, String> tracks = new HashMap<Integer, String>();

	@ManyToMany(mappedBy = "appearsOnCDs")
	private List<Artist> createdByArtists;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getCover() {
		return cover;
	}

	public void setCover(byte[] cover) {
		this.cover = cover;
	}

	public Map<Integer, String> getTracks() {
		return tracks;
	}

	public void setTracks(Map<Integer, String> tracks) {
		this.tracks = tracks;
	}

	public String getMusicCompany() {
		return musicCompany;
	}

	public void setMusicCompany(String musicCompany) {
		this.musicCompany = musicCompany;
	}

	public Integer getNumberOfCDs() {
		return numberOfCDs;
	}

	public void setNumberOfCDs(Integer numberOfCDs) {
		this.numberOfCDs = numberOfCDs;
	}

	public Float getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(Float totalDuration) {
		this.totalDuration = totalDuration;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public List<Artist> getCreatedByArtists() {
		return createdByArtists;
	}

	public void setCreatedByArtists(List<Artist> createdByArtists) {
		this.createdByArtists = createdByArtists;
	}

}
