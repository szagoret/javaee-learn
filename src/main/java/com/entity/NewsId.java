package com.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * 
 * @author szagoret Ambele clase NewsId si News reprezinta date a aceleasi
 *         tabele, in cazul acesta, entitatea News nu are nevoie de cheie
 *         explicita ID
 */
@Embeddable
public class NewsId implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;
	private String title;
	private String language;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
