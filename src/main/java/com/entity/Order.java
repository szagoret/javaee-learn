package com.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @OneToMany(fetch = FetchType.EAGER)
    // @JoinTable(name = "jnd_ord_line", joinColumns = @JoinColumn(name =
    // "order_fk"), inverseJoinColumns = @JoinColumn(name = "order_line_fk"))
    @JoinColumn(name = "order_fk")
    private List<OrderLine> orderLines;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Date getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
    }

    public List<OrderLine> getOrderLines() {
	return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
	this.orderLines = orderLines;
    }

}
