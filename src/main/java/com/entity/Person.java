package com.entity;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

@Entity
@Access(AccessType.FIELD)
@Table(name = "customer")
@NamedQueries({
		@NamedQuery(name = "findPersonKurchin", query = "SELECT p FROM Person p WHERE p.firstName = 'Kurchin'"),
		@NamedQuery(name = "getAllPersons", query = "SELECT p FROM Person p") })
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Size(min = 3, max = 15, message = "First Name size must be betwen 10 and 15 characters")
	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday")
	private Date birthDay;

	// @Embedded
	// @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.REMOVE })
	@JoinColumn(name = "address_fk", nullable = false)
	private Address address;

	// @Transient
	private Integer age;

	private String phoneNumber;

	@PrePersist
	@PreUpdate
	private void validate() {
		if (firstName == null || "".equals(firstName))
			throw new IllegalArgumentException("Invalid first name");
		if (lastName == null || "".equals(lastName))
			throw new IllegalArgumentException("Invalid last name");
	}

	@PostLoad
	@PostPersist
	@PostUpdate
	public void calculateAge() {
		if (birthDay == null) {
			age = null;
			return;
		}
		Calendar birth = new GregorianCalendar();
		birth.setTime(birthDay);
		Calendar now = new GregorianCalendar();
		now.setTime(new Date());
		int adjust = 0;
		if (now.get(Calendar.DAY_OF_YEAR) - birth.get(Calendar.DAY_OF_YEAR) < 0) {
			adjust = -1;
		}
		age = now.get(Calendar.YEAR) - birth.get(Calendar.YEAR) + adjust;
	}

	public Person() {
	}

	public Person(String firstName, String lastName, Date birthDay) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDay = birthDay;
	}

	// @Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "phone_number", length = 15)
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "{id:" + id + " firstName:" + firstName + " lastName:"
				+ lastName + " birthDay:" + birthDay + "}";
	}

}
