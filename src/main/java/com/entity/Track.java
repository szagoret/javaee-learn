package com.entity;

import javax.persistence.Basic;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/*@Entity
 @Table(name = "Tracks")*/
public class Track {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;
    private Float duration;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private byte[] wav;

    private String description;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public Float getDuration() {
	return duration;
    }

    public void setDuration(Float duration) {
	this.duration = duration;
    }

    public byte[] getWav() {
	return wav;
    }

    public void setWav(byte[] wav) {
	this.wav = wav;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

}
