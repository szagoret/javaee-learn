package com.listeners;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.entity.Item;

public class PriceValidationListener {

	@PrePersist
	@PreUpdate
	public void validatePrice(Item item) {
		if (item.getPrice() == null) {
			return;
		}

		if (item.getPrice() < 5F) {
			throw new IllegalArgumentException("Price is not good");
		}
	}
}
