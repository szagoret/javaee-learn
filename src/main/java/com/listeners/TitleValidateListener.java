package com.listeners;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

import com.entity.Item;

public class TitleValidateListener {

	@PostLoad
	@PostPersist
	@PostUpdate
	public void calculatePages(Item item) {
		if (item.getTitle().isEmpty()) {
			item.setTitle("Empty title");
		}
	}
}
