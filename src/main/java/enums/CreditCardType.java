package enums;

public enum CreditCardType {
    VISA, MASTER_CARD, AMERICAN_EXPRESS
}
