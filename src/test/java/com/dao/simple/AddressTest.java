package com.dao.simple;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.dao.AbstractTest;
import com.entity.Address;

public class AddressTest extends AbstractTest {
    @Ignore
    @Test
    public void testInsertAddress() throws Exception {
	Address address = new Address();
	address.setCity("London");
	address.setCountry("Great Britain");
	address.setState("England");
	address.setStreet("Wall Street");
	address.setZipcode("124");

	Address address1 = new Address();
	address1.setCity("San Francisco");
	address1.setCountry("USA");
	address1.setState("California");
	address1.setStreet("Begoin");
	address1.setZipcode("423");

	transaction.begin();
	manager.persist(address);
	manager.persist(address1);
	transaction.commit();

	Address result = manager.find(Address.class, 1L);
	Assert.assertEquals(result.getState(), "England");
    }

}
