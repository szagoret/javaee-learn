package com.dao.simple;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.dao.AbstractTest;
import com.entity.Person;

public class PersonTest extends AbstractTest {

    @Ignore
    @Test
    public void shouldFindPerson() throws Exception {

	Person person = manager.find(Person.class, 1);
	Assert.assertEquals("Valea", person.getFirstName());
    }

    @Ignore
    @Test
    public void shouldCreateTresurePerson() throws Exception {
	transaction.begin();
	Person person = new Person("Princels", "Elodie", new Date());
	manager.persist(person);
	transaction.commit();
	Assert.assertNotNull("Id not be empty", person.getId());
	person = manager.find(Person.class, person.getId());
	Assert.assertNotNull("Id not be empty", person.getId());
    }

    @Ignore
    @Test(expected = ConstraintViolationException.class)
    public void throwConstraintException() {
	transaction.begin();
	Person person = new Person("P", "Elodie", new Date());
	manager.persist(person);
    }
}
